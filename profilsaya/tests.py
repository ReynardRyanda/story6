from django.test import TestCase
from django.urls import resolve
from .views import *

# Create your tests here.


class Story7UnitTest(TestCase):

    def test_URL_exist(self):
        response = self.client.get('/story7/')
        self.assertEqual(200, response.status_code)

    def test_story7_function_is_used(self):
        response = resolve('/story7/')
        self.assertEqual(response.func, profilsaya)

    def test_story7_html_is_used(self):
        response = self.client.get('/story7/')
        self.assertTemplateUsed(response, 'profilsaya.html')
