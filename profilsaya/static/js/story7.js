
let lightMode = { "--background-color-higher": "#333", "--background-color-lower": "#272727", "--color-primary": "#FFF", "--color-secondary": "#ea80fc", "--color-ternary": "#03DAC6" }

let darkMode = { "--background-color-higher": "#FFF", "--background-color-lower": "#F5F5F5", "--color-primary": "#333", "--color-secondary": "#FF0000", "--color-ternary": "#3399cc" }

$(document).ready(function () {
    $("#light-dark-switch").on("click", () => {
        if ($("#light-dark-switch").is(":checked")) {
            $(":root").css(lightMode);
            $(".header-image-wrapper img").css("opacity", "0.8")
        } else {
            $(":root").css(darkMode);
            $(".header-image-wrapper img").css("opacity", "1")
        }
    });

    $(".title-container").click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).next().slideUp(200);
        } else {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
            $(".desc").slideUp(200);
            $(this).next().slideDown(200);
        }
    })
})