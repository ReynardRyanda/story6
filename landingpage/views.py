from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.


def landingpage(request):
    context = {
        'form': StatusForm(),
        'data': Status.objects.all()
    }
    return render(request, 'landingpage.html', context)


def tambah_status(request):
    if StatusForm(request.POST).is_valid():
        status_baru = Status.objects.create(status=request.POST['status'])
    return redirect('/')
