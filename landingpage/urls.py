from django.urls import path
from .views import *

app_name = 'landingpage'

urlpatterns = [
    path('',landingpage, name='landingpage'),
    path('tambah_status/',tambah_status, name='tambah_status'),
]